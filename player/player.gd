extends CharacterBody3D

@export var sensitive_x = 0.2
@export var sensitive_y = 0.2
@export var locomotionStatePlaybackPath: String;

@onready var character = $Character
@onready var camera_mount = $CameraMount
@onready var animation_player = $AnimationPlayer
@onready var animation_tree = $AnimationTree
@onready var right_hand_area = $Character/YBot/GeneralSkeleton/RightHand/RightHandArea
@onready var right_hand_collision = $Character/YBot/GeneralSkeleton/RightHand/RightHandArea/RightHandCollision
@onready var state_machine = animation_tree["parameters/LocomotionStateMachine/playback"] as AnimationNodeStateMachinePlayback

var is_running = false
var is_attacking = false
var is_attack_end2 = false
var is_attack_end = 0

const SPEED = 2.5
const RUN_SPEED = 6.0
const JUMP_VELOCITY = 4.5

const ANIM_WALK_FORWARD = "male-movement-anims/Walk_Forward"
const ANIM_RUN_FORWARD = "male-movement-anims/Run_Forward"
const ANIM_IDLE = "male-movement-anims/Idle"
const ANIM_RUN_TO_STOP = "male-movement-anims/Run_To_Stop"

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var hit_pos = Vector3(0, 0, 0)
var enemy_pos_vec = Vector3(0, 0, 0)
var enemy_basis_x = Vector3(0, 0, 0)


var punch_positions = []

var gap_collisions_area: Area3D = Area3D.new()
const GAP_STEPS = 3
const GAP_STEP_SIZE = 1.0 / GAP_STEPS
var collided = false
var fist_prev_transform: Transform3D = Transform3D()
var fist_prev_prev_transform: Transform3D = Transform3D()
var real_last_hit_start = Vector3(0, 0, 0)
var real_last_hit_end = Vector3(0, 0, 0)
var punch_original_trail: Array[Vector3] = []
var punch_gap_trail: Array[Vector3] = []

func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func _input(event):	
	if event is InputEventKey && event.is_action_pressed("ui_up"):
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	if event is InputEventKey && event.is_action_pressed("ui_down"):
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		
	if event is InputEventMouseMotion:
		rotate_y(deg_to_rad(-event.relative.x*sensitive_x))
		
		# Предотвращаем вращение персонажа вместе с камерой, если он стоит на месте
		# TODO: Вызывает jitter при движении и вращении камеры по оси Y, надо разобраться
		character.rotate_y(-deg_to_rad(-event.relative.x*sensitive_x))
		
		camera_mount.rotate_x(deg_to_rad(-event.relative.y*sensitive_y))

func _physics_process(delta):
	if is_attacking || is_attack_end2 || is_attack_end > 0:
		var collided = false
		if gap_collisions_area != null:
			var colliders = gap_collisions_area.get_overlapping_areas()
			if colliders.size() > 0:
				on_punch_hit_area(colliders[0])
				print(colliders)
				collided = true
				is_attacking = false

		if (is_attack_end == 0 || is_attack_end == 2) && !collided:
			build_gap_collisions()
		else:
			delete_children(gap_collisions_area)
		
		if is_attack_end > 0:
			is_attack_end = is_attack_end - 1

		is_attack_end2 = false;
	
	fist_prev_prev_transform = fist_prev_transform	
	fist_prev_transform = right_hand_collision.global_transform		
	
	if punch_positions.size() > 4:
		var start = punch_positions[-4]
		var end = punch_positions[-1]
		var start_to_end = end - start
		#DebugDraw.draw_line(start, start + (start_to_end.normalized()*5), Color(1, 0, 0))
	
	handle_movement(delta)

func on_punch_hit_area(area: Area3D):
	var parent_node = area.get_parent_node_3d()
	var character = area.find_parent("Character")
	
	if character == null:
		return 

	var enemy = character.get_parent()

	if enemy.name == "XbotEnemy":
		print("collided with enemy")
		var hit_direction = fist_prev_transform.origin - fist_prev_prev_transform.origin
		enemy.new_hit_with_direction(area, hit_direction)

func _on_right_hand_area_area_shape_entered(area_rid, area: Area3D, area_shape_index, local_shape_index):
	return
	print("right fist collision area", area.name)
	#return
	var parent_node = area.get_parent_node_3d()
	var character = area.find_parent("Character")
	
	if character == null:
		return 
		
	var enemy = character.get_parent()
	
	if enemy.name == "XbotEnemy":
		print("collided with enemy")
		return
		right_hand_collision.set_deferred("disabled", true)
		var hurt_shape = area.get_child(area_shape_index)

func on_gap_area_collide(area_rid, area: Area3D, area_shape_index, local_shape_index):
	var local_shape = gap_collisions_area.get_child(local_shape_index)
#	DebugDraw.draw_sphere(local_shape.global_transform.origin, 0.1, Color(1, 0, 0), 4)
	print("GAP AREA COLLIDE!")

func on_punch_right_started():
	if gap_collisions_area != null:
		gap_collisions_area.queue_free()
		
	gap_collisions_area = Area3D.new()
	gap_collisions_area.monitoring = true
	gap_collisions_area.connect("area_shape_entered", on_gap_area_collide)
	
	
	punch_original_trail.clear()
	punch_gap_trail.clear()
	is_attacking = true
	
func on_punch_right_finished():
	if is_attacking:
		is_attack_end = 2

	is_attacking = false
	
func build_gap_collisions():
#	if gap_collisions_area != null:
#		gap_collisions_area.queue_free()
#	gap_collisions_area = Area3D.new()

	delete_children(gap_collisions_area)
	
	var end_transform = right_hand_collision.global_transform
	
	punch_original_trail.clear()
	punch_gap_trail.clear()
	

	gap_collisions_area.set_collision_mask_value(1, false)
	gap_collisions_area.set_collision_mask_value(2, true)
	# Approach 1: default behavior (trail)
	for i in range(0, GAP_STEPS+1):
		var collision_shape = CollisionShape3D.new()
		#var capsule_shape = CapsuleShape3D.new()
		var capsule_shape = SphereShape3D.new()
		capsule_shape.radius = right_hand_collision.shape.radius
		collision_shape.shape = capsule_shape
		gap_collisions_area.add_child(collision_shape)
		add_child(gap_collisions_area)
		var start_transform = fist_prev_transform
		var middle_transform = start_transform.interpolate_with(end_transform, i*GAP_STEP_SIZE)

		# add punch debug trail sphere
		if i == 0 || i == GAP_STEPS:
			punch_original_trail.append(middle_transform.origin)
			#DebugDraw.draw_sphere(middle_transform.origin, capsule_shape.radius, Color(0, 1, 1), 2)
		else:
			punch_gap_trail.append(middle_transform.origin)
			#DebugDraw.draw_sphere(middle_transform.origin, capsule_shape.radius, Color(1, 1, 0), 2)

		collision_shape.set_global_transform(middle_transform)

func handle_movement(delta):
	
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		velocity.y = JUMP_VELOCITY
	
	if Input.is_action_just_pressed("attack"):
		punch_positions.clear()
		state_machine.travel("PunchRight")
		
	if Input.is_action_just_pressed("heavy_attack"):
		punch_positions.clear()
		state_machine.travel("PunchBodyRight")
		
	if Input.is_action_just_pressed("high_attack"):
		punch_positions.clear()
		state_machine.travel("PunchHeadRight")
				
	if Input.is_action_just_pressed("run"):
		is_running = true
	elif Input.is_action_just_released("run"):
		is_running = false

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("left", "right", "forward", "backward")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	var current_speed = SPEED
	
	if is_running:
		current_speed = RUN_SPEED
	
	if direction:
		velocity.x = direction.x * current_speed
		velocity.z = direction.z * current_speed
		character.look_at(position + direction)

		if !is_running:
			state_machine.travel("Walk")
			
		if is_running:
			state_machine.travel("Run")
	else:
		if state_machine.get_current_node() != "Idle":
			state_machine.travel("Idle")
			
		# TODO: difference between this:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)
		# and this:
		#velocity.x = 0
		#velocity.z = 0
	move_and_slide()
	
func delete_children(node):
	for n in node.get_children():
		node.remove_child(n)
		n.queue_free()
