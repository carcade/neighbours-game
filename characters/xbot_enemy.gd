extends CharacterBody3D

@onready var animation_player = $AnimationPlayer
@onready var upper_chest_bone_collision = $Character/XBot/GeneralSkeleton/UpperChestBoneAttachment/UpperChestBoneArea/UpperChestBoneCollision
@onready var audio_stream_player_3d = $AudioStreamPlayer3D

const debug = true

var is_getting_hit = false
var last_hurt_start = Vector3(0, 0, 0)
var last_hurt_end = Vector3(0, 0, 0)
var last_hit_start = Vector3(0, 0, 0)
var last_hit_end = Vector3(0, 0, 0)
var last_hit2_start = Vector3(0, 0, 0)
var last_hit2_end = Vector3(0, 0, 0)
var last_hit_position = Vector3(0, 0, 0)
var last_hurt_left_start = Vector3(0, 0, 0)
var last_hurt_left_end = Vector3(0, 0, 0)

var punch_vector_start = Vector3(0, 0, 0)
var punch_vector_end = Vector3(0, 0, 0)

func _ready():
	animation_player.play("male-movement-anims/Idle")

func _process(delta):
	if debug:
		DebugDraw.draw_line(last_hurt_start, last_hurt_end, Color(0, 1, 0))
		DebugDraw.draw_line(last_hurt_left_start, last_hurt_left_end, Color(0, 1, 0))
		#DebugDraw.draw_line(last_hit_start, last_hit_end, Color(1, 0, 0))
		DebugDraw.draw_line(last_hit2_start, last_hit2_end, Color(0, 1, 1))
		DebugDraw.draw_sphere(last_hit_position, 0.05, Color(1, 0, 0))
		
		DebugDraw.draw_line(punch_vector_start, punch_vector_end, Color(1, 0, 1))
		
		var space_state = get_world_3d().direct_space_state
		var origin = Vector3(0, 0, 0)
		var end = upper_chest_bone_collision.global_transform.origin
		var query = PhysicsRayQueryParameters3D.create(origin, end)
		query.collide_with_areas = true
		query.exclude = [self]
		var result = space_state.intersect_ray(query)
		#print(result)

func hit(area: Area3D):
	#print("enemy got hit")
	if !is_getting_hit && animation_player.current_animation != "hit-anims/HeadHit_L_remap" && animation_player.current_animation != "hit-anims/HeadHit_R_remap":
		if area.name == "HeadBoneArea":
			var is_getting_hit = true
			animation_player.play("hit-anims/HeadHit_L_remap")
		elif area.name == "UpperChestBoneArea":
			var is_getting_hit = true
			animation_player.play("hit-anims/HeadHit_R_remap")

func hit_with_direction(area: Area3D, shape: CollisionShape3D, hit_collision_shape: CollisionShape3D, fist_vector: Vector3):
	punch_vector_start = hit_collision_shape.global_transform.origin
	punch_vector_end = hit_collision_shape.global_transform.origin + fist_vector.normalized()
	
	print("hit with direction: ", area, hit_collision_shape)
	var hurt_shape_position = shape.global_transform.origin
	# Identity vector pointing forward from hurtbox collision
	var hurt_shape_front_vector = Vector2(shape.global_transform.basis.z.x, shape.global_transform.basis.z.z).normalized()
	var hurt_shape_left_vector = Vector2(shape.global_transform.basis.x.x, shape.global_transform.basis.x.z).normalized()
	# Identity vector pointing from hurtbox collision for hit pos
	var hurt_to_hit_vector3 = hit_collision_shape.global_transform.origin - shape.global_transform.origin
	var hurt_to_hit_vector = Vector2(hurt_to_hit_vector3.x, hurt_to_hit_vector3.z).normalized()
	
	var hurt_front_to_hit_dot = hurt_shape_front_vector.dot(hurt_to_hit_vector)
	var hurt_left_to_hit_dot = hurt_shape_left_vector.dot(hurt_to_hit_vector)
	print("hurt front to hit dot: ", hurt_front_to_hit_dot)
	print("hurt left to hit dot: ", hurt_left_to_hit_dot)
	
	if area.name == "UpperChestBoneArea":
		if abs(hurt_front_to_hit_dot) >= abs(hurt_left_to_hit_dot):
			if hurt_front_to_hit_dot >= 0.5:
				print("hit body front")
				animation_player.play("fighting-anims-3/HitBody_F_IP_remap")
			elif hurt_front_to_hit_dot <= -0.5:
				print("hit body back")
				animation_player.play("fighting-anims-3/HitBack_remap")
		else:
			if hurt_left_to_hit_dot >= 0.5:
				print("hit body left")
				animation_player.play("fighting-anims-3/HitBody_L_IP_remap")
			elif hurt_left_to_hit_dot <= -0.5:
				print("hit body right")
				animation_player.play("fighting-anims-3/HitBody_R_IP_remap")
	elif area.name == "HeadBoneArea":
		if abs(hurt_front_to_hit_dot) >= abs(hurt_left_to_hit_dot):
			if hurt_front_to_hit_dot >= 0.5:
				print("hit head front")
				animation_player.play("fighting-anims-3/HitHead_F_IP_remap")
			elif hurt_front_to_hit_dot <= -0.5:
				print("hit head back")
				animation_player.play("fighting-anims-3/HitBack_remap")
		else:
			if hurt_left_to_hit_dot >= 0.5:
				print("hit head left")
				animation_player.play("fighting-anims-3/HitHead_L_IP_remap")
			elif hurt_left_to_hit_dot <= -0.5:
				print("hit head right")
				animation_player.play("fighting-anims-3/HitHead_R_IP_remap")
	
	# DEBUG
	var hurt_y = hit_collision_shape.global_transform.origin.y
	last_hurt_start = Vector3(shape.global_transform.origin.x, hurt_y, shape.global_transform.origin.z)
	last_hurt_end = Vector3(shape.global_transform.origin.x + hurt_shape_front_vector.x, hurt_y, shape.global_transform.origin.z + hurt_shape_front_vector.y)
	last_hit_start = last_hurt_start
	last_hit_end = Vector3(last_hit_start.x + hurt_to_hit_vector.x, hurt_y, last_hit_start.z + hurt_to_hit_vector.y)
	
	# hurbox left vector
	last_hurt_left_start = last_hurt_start
	last_hurt_left_end = Vector3(last_hurt_start.x + hurt_shape_left_vector.x, hurt_y, last_hurt_start.z + hurt_shape_left_vector.y)
	
	# collision hit pos
	last_hit_position = hit_collision_shape.global_transform.origin
	
	# Second approach
	var hit_direction = -Vector2(hit_collision_shape.global_transform.basis.y.x, hit_collision_shape.global_transform.basis.y.z).normalized()
	last_hit2_start = last_hurt_start
	last_hit2_end = Vector3(last_hit2_start.x + hit_direction.x, hurt_y, last_hit2_start.z + hit_direction.y)
	
	# Third approach (ray-casting)
	var space_state = get_world_3d().direct_space_state
	var origin = hit_collision_shape.global_transform.origin
	var end = shape.global_transform.origin
	var query = PhysicsRayQueryParameters3D.create(origin, end)
	query.collide_with_areas = true
	query.exclude = [self]
	var result = space_state.intersect_ray(query)
	print(result)
	pass

func new_hit_with_direction(area: Area3D, hit_direction: Vector3):
	audio_stream_player_3d.play()
	var shape = area.get_child(0) as CollisionShape3D
	print(shape, hit_direction)
	DebugDraw.draw_line(shape.global_transform.origin, shape.global_transform.origin - (hit_direction.normalized()*3), Color(1, 0, 1), 2)
	
	# Identity vector pointing forward from hurtbox collision
	var hurt_shape_front_vector = Vector2(shape.global_transform.basis.z.x, shape.global_transform.basis.z.z).normalized()
	var hurt_shape_left_vector = Vector2(shape.global_transform.basis.x.x, shape.global_transform.basis.x.z).normalized()
	# Identity vector pointing from hurtbox collision for hit pos
	var hurt_to_hit_vector3 = -hit_direction
	var hurt_to_hit_vector = Vector2(hurt_to_hit_vector3.x, hurt_to_hit_vector3.z).normalized()
	
	var hurt_front_to_hit_dot = hurt_shape_front_vector.dot(hurt_to_hit_vector)
	var hurt_left_to_hit_dot = hurt_shape_left_vector.dot(hurt_to_hit_vector)
	
	if area.name == "UpperChestBoneArea":
		if abs(hurt_front_to_hit_dot) >= abs(hurt_left_to_hit_dot):
			if hurt_front_to_hit_dot >= 0.5:
				print("hit body front")
				animation_player.play("fighting-anims-3/HitBody_F_IP_remap")
			elif hurt_front_to_hit_dot <= -0.5:
				print("hit body back")
				animation_player.play("fighting-anims-3/HitBack_remap")
		else:
			if hurt_left_to_hit_dot >= 0.5:
				print("hit body left")
				animation_player.play("fighting-anims-3/HitBody_L_IP_remap")
			elif hurt_left_to_hit_dot <= -0.5:
				print("hit body right")
				animation_player.play("fighting-anims-3/HitBody_R_IP_remap")
	elif area.name == "HeadBoneArea":
		if abs(hurt_front_to_hit_dot) >= abs(hurt_left_to_hit_dot):
			if hurt_front_to_hit_dot >= 0.5:
				print("hit head front")
				animation_player.play("fighting-anims-3/HitHead_F_IP_remap")
			elif hurt_front_to_hit_dot <= -0.5:
				print("hit head back")
				animation_player.play("fighting-anims-3/HitBack_remap")
		else:
			if hurt_left_to_hit_dot >= 0.5:
				print("hit head left")
				animation_player.play("fighting-anims-3/HitHead_L_IP_remap")
			elif hurt_left_to_hit_dot <= -0.5:
				print("hit head right")
				animation_player.play("fighting-anims-3/HitHead_R_IP_remap")
	

func _on_animation_player_animation_finished(anim_name: String):
	if anim_name == "hit-anims/HeadHit_R_remap" || anim_name == "hit-anims/HeadHit_L_remap":
		#print("fight anim finished")
		is_getting_hit = false
	pass # Replace with function body.
