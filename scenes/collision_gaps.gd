extends Node3D

@onready var start_collision_shape_3d = $StartArea3D/StartCollisionShape3D
@onready var end_collision_shape_3d = $EndArea3D/EndCollisionShape3D

const GAP_STEPS = 10
const GAP_STEP_SIZE = 1.0 / GAP_STEPS
var area_node = Area3D.new()
var collided = false

#func _ready():
	

func _physics_process(delta):
	if !collided:
		end_collision_shape_3d.global_translate(Vector3(-0.01, 0, 0))
		var colliders = area_node.get_overlapping_bodies()
		print(colliders)
		
		if colliders.size() > 0:
			collided = true
	
	
	if !collided:
		build_gap_collision()
	elif area_node != null:
		area_node.queue_free()

func on_collide():
	print("collided!")
	
func build_gap_collision():
	area_node.queue_free()
	area_node = Area3D.new()
	for i in range(1, GAP_STEPS+1):
		var collision_shape = CollisionShape3D.new()
		var capsule_shape = CapsuleShape3D.new()
		collision_shape.shape = capsule_shape
		area_node.add_child(collision_shape)
		add_child(area_node)
		var start_transform = start_collision_shape_3d.global_transform
		var end_transform = end_collision_shape_3d.global_transform
		var middle_transform = start_transform.interpolate_with(end_transform, i*GAP_STEP_SIZE)
		collision_shape.set_global_transform(middle_transform)
